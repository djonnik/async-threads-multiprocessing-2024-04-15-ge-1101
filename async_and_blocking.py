import time
import asyncio
from numbers import Number


def func_1(x_in: Number) -> str:
    print("func_1 started")
    time.sleep(2)
    return f"func_1({x_in}): {pow(x_in, 2)}"


async def func_2(x_in: Number) -> str:
    print("func_2 started")
    await asyncio.sleep(2)
    return f"func_2({x_in}): {pow(x_in, 0.5)}"


async def main(x_in: Number) -> None:
    print("before create_task ...")
    task_1 = asyncio.create_task(asyncio.to_thread(func_1, x_in))
    # task_1 = asyncio.create_task(func_1(x_in))
    task_2 = asyncio.create_task(func_2(x_in))
    print("after create_task ...")
    res_1 = await task_1
    # res_1 = func_1(x_in)
    # res_1 = await func_1(x_in)
    print(res_1)
    res_2 = await task_2
    # res_2 = await func_2(x_in)
    print(res_2)


if __name__ == "__main__":
    x = 9
    start = time.perf_counter()
    asyncio.run(main(x))
    duration = time.perf_counter() - start
    print(f"Total code execution time: {duration}")
